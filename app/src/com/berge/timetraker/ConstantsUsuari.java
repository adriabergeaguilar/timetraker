package com.berge.timetraker;

/**
 * Esta clase es la que representa todos los datos que el usuario puede editar su valor
 * para poder modificar asi el comportamiento de la aplicacion
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     com.berge.timetraker.models.Actividad
 */
public class ConstantsUsuari {
    /**
     * instancia para generar el patron singleton
     */
    public static ConstantsUsuari instance;
    public static ConstantsUsuari getInstance(){
        if(instance==null){
            instance = new ConstantsUsuari();
        }
        return instance;
    }

    /**
     * variable que representa cada cuantos segundos se ara el recuento del tiempo
     */
    private int segonsRecompte = 1;

    public int getSegonsRecompte() {
        return segonsRecompte;
    }

    public void setSegonsRecompte(int segonsRecompte) {
        this.segonsRecompte = segonsRecompte;
        Log.info("temps de reconta usuari : " + segonsRecompte);
    }
}

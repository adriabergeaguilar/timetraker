package com.berge.timetraker.utils;

import com.berge.timetraker.models.decorator.DecoradorLimitedTime;
import com.berge.timetraker.models.decorator.DecoradorProgramTime;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by adria on 13/10/14.
 */
public class Utils {
    public static boolean isEqualsDay(Date day1, Date day2){
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(day1);
        cal2.setTime(day2);
        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
        return sameDay;
    }

    public static void daysOfWeek(DecoradorProgramTime programTime, int hora, int min){
        int[] dias = new int[]{
                Calendar.MONDAY,
                Calendar.SUNDAY,
                Calendar.FRIDAY,
                Calendar.SATURDAY,
                Calendar.THURSDAY,
                Calendar.TUESDAY,
                Calendar.WEDNESDAY
        };
        for(int i : dias) {
            Calendar date = Calendar.getInstance();
            date.set(Calendar.DAY_OF_WEEK, i);
            //date.set(Calendar.AM_PM, hora > 12 ? Calendar.PM : Calendar.AM);
            date.set(Calendar.HOUR_OF_DAY, hora);
            date.set(Calendar.MINUTE, min);
            date.set(Calendar.SECOND, 0);
            date.set(Calendar.MILLISECOND, 0);

            programTime.addDay(date.getTime());
        }

    }

    public static void daysOfWeekLimited(DecoradorLimitedTime decoradorLimitedTime, int limited) {
        int[] dias = new int[]{
                Calendar.MONDAY,
                Calendar.SUNDAY,
                Calendar.FRIDAY,
                Calendar.SATURDAY,
                Calendar.THURSDAY,
                Calendar.TUESDAY,
                Calendar.WEDNESDAY
        };
        for(int i : dias) {
            decoradorLimitedTime.addLimiteds(i,limited);
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
}

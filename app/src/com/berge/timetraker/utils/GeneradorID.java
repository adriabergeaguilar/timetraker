package com.berge.timetraker.utils;

import com.berge.timetraker.Log;
import com.berge.timetraker.models.Actividad;
import com.berge.timetraker.models.Projecte;

import java.util.HashMap;
import java.util.Random;

/**
 * Esta clase es la que gestiona las id's del sistema
 * gestionando de esta manera de que no se puedan repetir las id's
 * aun que ahora mismo si cerramos el programa vuelve a empezar desde 0
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 */
public class GeneradorID {
    /**
     * instancia de la clase para seguir el patron singleton
     */
    private static GeneradorID generador = null;

    public static GeneradorID getInstancia(){
        if(generador == null){
            generador = new GeneradorID();
        }
        return generador;
    }

    /**
     * Hash de los proyectos con su cantador para popder generar las distintas id's
     */
    private HashMap<Projecte,Integer> ids = new HashMap<Projecte,Integer>();

    /**
     * devuelve el siguiente id del proyecto que sera el padre,
     * de esta forma generamos el arbol de ids como
     * 1.1.2
     * 1.1.3
     * 1.2
     * 2.1
     * @param padre
     * @param hijo
     * @return
     * @throws Exception
     */
    public int getID(Projecte padre,Actividad hijo) throws Exception {
        int count = 0;
        if(ids.containsKey(padre)){
            count = ids.get(padre);
            count++;
            ids.put(padre,count);
            if(hijo instanceof Projecte){
                Projecte p = (Projecte) hijo;
                ids.put(p,0);
            }

        }else{
            ids.put(padre,0);
            count = getID(padre,hijo);
        }
        Log.info("id creado : " + count + " en el proyecto " + padre.getId());
        return count;
    }

    /**
     * contador temporal de id's por si no se ponen en un sistema en arbol
     */
    private int countTemp = 0;
    public int getSimpleID() {
        countTemp++;
        Log.info("id temporal : " + countTemp);
        return countTemp;
    }
}

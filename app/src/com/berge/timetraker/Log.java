package com.berge.timetraker;

import org.apache.logging.log4j.LogManager;

/**
 * Esta clase es la unica que tiene el objeto Logger de apache,
 * de esta manera la configuracion de ella la hacemos en ella y
 * el resto del proyecto simplemente deve llamar a las funciones
 * que son transparentes, de esta manera mostramos la informacion
 * de log en un fichero o por la consola
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     org.apache.logging.log4j.Logger
 */
public class Log {
    /**
     * instancia para generar la funcionalidad singleton
     */
    private static org.apache.logging.log4j.Logger log;

    static {
        log = LogManager.getLogger();
    }

    public static void info(String s) {
        log.info(s);
    }

    public static void debug(String s) {
        log.debug(s);
    }

    public static void error(String s){
        log.error(s);
    }

    public static void warn(String s){
        log.warn(s);
    }
}

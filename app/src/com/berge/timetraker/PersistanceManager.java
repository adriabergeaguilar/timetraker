package com.berge.timetraker;

import com.berge.timetraker.models.Actividad;
import com.berge.timetraker.models.Projecte;

import java.io.*;

/**
 * Created by Adria on 18/10/2014.
 */
public class PersistanceManager {
    /**
     * funcionalidad para serializar todo un arbol en un archivo
     * @param actividad
     * @throws IOException
     */
    public static void save(Actividad actividad) throws IOException {

        File file = new File("/home/adria/Descargas/projects.ser");
        if(!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fileOut =
                new FileOutputStream("/home/adria/Descargas/projects.ser");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(actividad);
        out.close();
        fileOut.close();

        Log.debug(file.getAbsolutePath() + "saved");
    }

    /**
     * funcion para desserializacion de todo un arbol
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Projecte reed() throws IOException, ClassNotFoundException {
        Projecte e = null;
        FileInputStream fileIn = new FileInputStream("/home/adria/Descargas/projects.ser");
        Log.debug(fileIn.getFD().toString() + "recuperado");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        e = (Projecte) in.readObject();
        in.close();
        fileIn.close();
        return e;
    }
}

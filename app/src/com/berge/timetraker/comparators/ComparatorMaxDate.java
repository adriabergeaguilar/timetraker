package com.berge.timetraker.comparators;

import java.util.Comparator;
import java.util.Date;
/**
 *Esta clase implementa un comparador de fechas para poder ordenarlas
 * de mayor a menor
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     Comparator
 * @see     Date
 */
public class ComparatorMaxDate implements Comparator<Date> {
    @Override
    public int compare(Date o1, Date o2) {
        int compare = -1;
        if(o1 != null && o2 != null) {
            if (o1.getTime() > o2.getTime()) {
                compare = 1;
            } else if (o1.getTime() == o2.getTime()) {
                compare = 0;
            } else {
                compare = -1;
            }
        }
        return compare;
    }
}

package com.berge.timetraker;

import com.berge.timetraker.models.Interval;
import com.berge.timetraker.models.Task;
import com.berge.timetraker.models.decorator.ProgramTask;

import java.io.Serializable;
import java.util.*;

/**
 * Rellotge representa a el contador de tiempo que contara segun el que contenga la configuracion
 * de la tarea que se le pide que cronometre, siendo la segunda parte del Observer con el Intervalo
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     com.berge.timetraker.models.Interval
 * @see     java.util.Observable
 * @see     java.io.Serializable
 * @see     java.util.Timer
 */
public class Rellotge extends Observable implements Serializable {
    /**
     * llista singleton de proyecto con su reloj, para poder tenerlo en ejecucion varios al mismo tiempo
     * pero solo un reloj por tarea.
     */
    private static HashMap<ProgramTask,Rellotge> llistat;

    /**
     * devuelve la instancia de la clase reloj para la tarea seleccionada
     * @param task
     * @return
     */
    public static Rellotge getInstance(ProgramTask task){
        return getInstance(task,false);
    }

    public static Rellotge getInstance(ProgramTask programTask, boolean constructor) {
        if(llistat == null){
            llistat = new HashMap<ProgramTask, Rellotge>();
        }
        Rellotge rellotge = llistat.get(programTask);

        if(rellotge == null && constructor){
            rellotge = new Rellotge();
            llistat.put(programTask,rellotge);
        }

        return rellotge;
    }
    public static void removed(ProgramTask programTask) {
        if(llistat != null){
            llistat.remove(programTask);
        }
    }

    /**
     * interval que se esta contando en este momento
     */
    private Interval interval;
    /**
     * tarea que se esta contabilizando
     */
    private ProgramTask tasks;
    /**
     * thread que se ejecuta cada X segundos que son los que el usuario configura
     */
    private Timer timer;

    private Rellotge(){
        Log.debug("new Rellotge");
    }

    /**
     * Esta funcion le da una tarea al reloj para que programe
     * @param programTask
     * @throws Exception
     */
    public void addTaska(ProgramTask programTask) throws Exception {

        tasks = programTask;
        if(programTask != null) {
            Log.debug("add Task to Clock : " + programTask.getName() + " - " + programTask.getClass().getName());
            if(timer == null) {
                timer = new Timer();
            }
            interval = new Interval(new Date());
            this.addObserver(interval);
            programTask.insertInterval(interval);
            interval.setParent(programTask);


            Date firstTime = programTask.getFirstTime();
            int period = programTask.getPeriod();

            Log.debug("first time : " + firstTime + "\t" + "periode : " + period);
            timer.schedule(new ClickRellotge(),firstTime,period);

        }
    }

    /**
     * esta funcion elimina la programacion de una tarea
     * consiguiendo asi que no seguir contando esa tarea
     * @param programTask
     */
    public void removedTask(ProgramTask programTask){

        if(interval != null){
            //interval.setFin(new Date());
            tick(new Date(),false);
        }
        for(Interval interval : programTask.getIntervals()) {
            this.deleteObserver(interval);
        }
        if(timer != null) {
            timer.cancel();
            timer.purge();

            Log.debug("delete Task to Clock : " + programTask.getName());
        }

        timer = null;
        interval = null;
        tasks = null;
    }

    /**
     * esta funcion es la que se ejecuta cada vez que pasa un periodo de tiempo
     * marcado por el usuario o por la tarea
     * @param date
     * @param first
     */
    private void tick(Date date,boolean first){
        if(first){
            if(interval != null) {
                interval.setInit(date);
            }
        }
        if(tasks != null) {
            Log.debug("tick : " + tasks.getName() + " date : " + date);
        }
        //System.out.println("Debug:\t" + tasks.getName() );
        setChanged();
        notifyObservers(date);
    }




    /**
     * esta clase representa la tarea que ejecuta el Timer cada cierto
     * periodo de tiempo, que lo unico que hace es delegar dicha funcion
     * a tick
     */
    private class ClickRellotge extends TimerTask{
        private boolean first = true;
        @Override
        public void run() {
            Log.info("tick("+new Date()+")");
            tick(new Date(),first);
            if(first){
                first = false;
            }

        }

    }

}

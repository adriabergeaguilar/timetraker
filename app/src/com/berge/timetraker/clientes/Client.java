package com.berge.timetraker.clientes;

import com.berge.timetraker.ConstantsUsuari;
import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.Task;
import com.berge.timetraker.models.visitor.Visitante;
import com.berge.timetraker.models.visitor.VisitanteTable;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by adria on 8/10/14.
 */
public class Client {
    private static Projecte base;
    private static final int UPDATE_UI_TIME = 2;
    public static void main(String[] args) throws Exception {


        Timer timer = new Timer();
        timer.schedule(new UITask(), UPDATE_UI_TIME * 1000, UPDATE_UI_TIME * 1000);

        ConstantsUsuari constantsUsuari = ConstantsUsuari.getInstance();
        constantsUsuari.setSegonsRecompte(UPDATE_UI_TIME);

        base = new Projecte(".",".");
        Projecte projecte1 = new Projecte("P1","Primer projecte");
        Projecte projecte2 = new Projecte("P2","Segon projecte");
        Task task1 = new Task("T1","primera taska de P2");
        Task task2 = new Task("T2","segona taska de P2");
        Task task3 = new Task("T3","primera taska de P1");

        base.addActividad(projecte1);

        projecte1.addActividad(task3);
        projecte1.addActividad(projecte2);

        projecte2.addActividad(task1);
        projecte2.addActividad(task2);



        task3.init();

        Thread.sleep(3 * 1000);

        task3.stop();

        Thread.sleep(7 * 1000);

        task2.init();

        Thread.sleep(10 * 1000);

        task2.stop();
        task3.init();

        Thread.sleep(2 * 1000);

        task3.stop();

        Thread.sleep(2 * 1000);

        timer.purge();
        timer.cancel();
    }

    private static class UITask extends TimerTask{

        @Override
        public void run() {
            if(base != null) {
                Visitante visitante = new VisitanteTable();
                base.acceptVisitante(visitante);
                System.out.println();
            }
        }
    }
}

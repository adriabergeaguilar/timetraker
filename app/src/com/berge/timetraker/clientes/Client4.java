package com.berge.timetraker.clientes;

import com.berge.timetraker.PersistanceManager;
import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.visitor.Visitante;
import com.berge.timetraker.models.visitor.VisitanteTable;

import java.io.IOException;

/**
 * Created by Adria on 18/10/2014.
 */
public class Client4 {
    public static void main(String[] args) {
        Projecte base = null;
        try {
            base = PersistanceManager.reed();
            if(base != null) {
                Visitante visitante = new VisitanteTable();
                base.acceptVisitante(visitante);
            }
        } catch (IOException e) {
            System.out.println("no existe fichero");
        } catch (ClassNotFoundException e) {
            System.out.println("el contenido no es correcto");
        }


    }
}

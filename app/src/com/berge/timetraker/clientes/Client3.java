package com.berge.timetraker.clientes;

import com.berge.timetraker.PersistanceManager;
import com.berge.timetraker.models.Actividad;
import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.Task;
import com.berge.timetraker.models.decorator.DecoradorLimitedTime;
import com.berge.timetraker.models.decorator.DecoradorNextTask;
import com.berge.timetraker.models.decorator.DecoradorProgramTime;
import com.berge.timetraker.models.decorator.ProgramTask;
import com.berge.timetraker.models.visitor.Visitante;
import com.berge.timetraker.models.visitor.VisitanteConsole;
import com.berge.timetraker.models.visitor.VisitanteTable;
import com.berge.timetraker.utils.Utils;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by adria on 9/10/14.
 */
public class Client3 {
        private static Projecte base;
        private static final int UPDATE_UI_TIME = 2;

        public static void main(String args[]) throws Exception {

            Timer timer = new Timer();
            timer.schedule(new UITask(), UPDATE_UI_TIME * 1000, UPDATE_UI_TIME * 1000);

            base = getStructureDecorada();

            ProgramTask[] tasks = searchTask(base);

            init(tasks);

            Thread.sleep(2 * 60 * 1000);

            stop(tasks);

            timer.purge();
            timer.cancel();

            PersistanceManager.save(base);
        }



        private static Projecte getStructureDecorada(){
            Projecte base = new Projecte("BASE","BASE");
            Projecte parent = base;
            Projecte hijo = null;

            for(int i = 0; i != 10; i++){
                hijo = new Projecte("P" + i,"descripcion" + i);
                parent.addActividad(hijo);

                parent = hijo;
            }
            //Projecte projecte = new Projecte("nom","desc");
            Task task1 = new Task("T1","descT");
            Task task2 = new Task("T2","desc");
            Task task3 = new Task("T3","t3");

            DecoradorLimitedTime limited = new DecoradorLimitedTime(task1);
            DecoradorProgramTime programTime = new DecoradorProgramTime(limited);
            DecoradorNextTask nextTask = new DecoradorNextTask(programTime);

            DecoradorLimitedTime limited2 = new DecoradorLimitedTime(task2);
            nextTask.setNextTask(task3);

            long milis = System.currentTimeMillis();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milis);
            long hora = calendar.get(Calendar.HOUR_OF_DAY);
            long minutes = calendar.get(Calendar.MINUTE);

            minutes += 1;
            if(minutes > 59){
                minutes = 0;
                hora += 1;
            }

            Utils.daysOfWeek(programTime,(int)hora,(int)minutes);
            Utils.daysOfWeekLimited(limited, 5 * 1000);
            Utils.daysOfWeekLimited(limited2, 5 * 1000);

            parent.addActividad(nextTask);
            parent.addActividad(limited2);
            hijo.addActividad(task3);

            return base;
        }

        private static Projecte getStructure(){
            Projecte base = new Projecte("Base","Base");
            Projecte projecte1 = new Projecte("P1","projecte1");
            Projecte projecte11 = new Projecte("P2","projecte2");
            Projecte projecte2 = new Projecte("P3","projecte3");
            Projecte projecte21 = new Projecte("P4","projecte4");
            Projecte projecte22 = new Projecte("P5","projecte5");

            Task task1 = new Task("T1", "taska 1");
            Task task2 = new Task("T2", "taska 1");
            Task task3 = new Task("T3", "taska 1");
            Task task4 = new Task("T4", "taska 1");
            Task task5 = new Task("T5", "taska 1");
            Task task6 = new Task("T6", "taska 1");
            Task task7 = new Task("T7", "taska 1");

            base.addActividad(projecte1);
            base.addActividad(projecte2);

            projecte1.addActividad(task1);
            projecte1.addActividad(projecte11);

            projecte11.addActividad(task2);
            projecte11.addActividad(task7);
            projecte2.addActividad(task3);
            projecte2.addActividad(projecte21);
            projecte2.addActividad(task6);
            projecte21.addActividad(task4);
            projecte21.addActividad(projecte22);
            projecte22.addActividad(task5);




            return base;
        }

        private static ProgramTask[] searchTask(Projecte projecte){
            ArrayList<ProgramTask> tasks = new ArrayList<ProgramTask>();
            for(Actividad actividad : projecte.getActividades()){
                if(actividad instanceof Task){
                    tasks.add((Task) actividad);
                }else if(actividad instanceof Projecte){
                    ProgramTask[] t = searchTask((Projecte)actividad);
                    for(ProgramTask tt : t){
                        tasks.add(tt);
                    }
                }else{
                    tasks.add((ProgramTask)actividad);
                }
            }
            return tasks.toArray(new ProgramTask[0]);
        }

    private static void init(ProgramTask... elements) throws Exception {
        for(ProgramTask t : elements){
            if(t != null && !t.getName().equals("T3")) {
                t.init();
            }
        }
    }

    private static void stop(ProgramTask... elements) throws Exception {
        for(ProgramTask t : elements){
            if(t != null && !t.getName().equals("T3")) {
                t.stop();
            }
        }
    }

    private static class UITask extends TimerTask {

        @Override
        public void run() {
            if(base != null) {
                Visitante visitante = new VisitanteConsole();
                base.acceptVisitante(visitante);
                System.out.println();
                System.out.println();
            }
        }
    }
}

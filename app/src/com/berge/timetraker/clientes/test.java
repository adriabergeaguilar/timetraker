package com.berge.timetraker.clientes;


import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.Task;
import com.berge.timetraker.models.decorator.DecoradorLimitedTime;
import com.berge.timetraker.models.decorator.DecoradorNextTask;
import com.berge.timetraker.models.decorator.DecoradorProgramTime;
import com.berge.timetraker.models.visitor.Visitante;
import com.berge.timetraker.models.visitor.VisitanteInforme;
import com.berge.timetraker.utils.Utils;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Adria on 17/10/2014.
 */
public class test {
    public static void main(String[] args) throws Exception {
        Projecte base = getStructureDecorada();
        Visitante visitante = new VisitanteInforme(new Date(),new Date());
        base.acceptVisitante(visitante);
    }

    private static Projecte getStructureDecorada() throws Exception {
        Projecte base = new Projecte("BASE","BASE");
        Projecte parent = base;
        Projecte hijo = null;

        for(int i = 0; i != 10; i++){
            hijo = new Projecte("P" + i,"descripcion" + i);
            parent.addActividad(hijo);

            parent = hijo;
        }
        //Projecte projecte = new Projecte("nom","desc");
        Task task1 = new Task("T1","descT");
        Task task2 = new Task("T2","desc");
        Task task3 = new Task("T3","t3");

        DecoradorLimitedTime limited = new DecoradorLimitedTime(task1);
        DecoradorProgramTime programTime = new DecoradorProgramTime(limited);
        DecoradorNextTask nextTask = new DecoradorNextTask(programTime);

        DecoradorLimitedTime limited2 = new DecoradorLimitedTime(task2);
        nextTask.setNextTask(task3);

        long milis = System.currentTimeMillis();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milis);
        long hora = calendar.get(Calendar.HOUR_OF_DAY);
        long minutes = calendar.get(Calendar.MINUTE);

        minutes += 2;
        if(minutes > 59){
            minutes = 0;
            hora += 1;
        }

        Utils.daysOfWeek(programTime, (int) hora, (int) minutes);
        Utils.daysOfWeekLimited(limited, 5 * 1000);
        Utils.daysOfWeekLimited(limited2, 5 * 1000);

        parent.addActividad(nextTask);
        parent.addActividad(limited2);
        hijo.addActividad(task3);

        nextTask.init();
        limited2.init();
        task3.init();

        Thread.sleep(60 * 1000);

        return base;
    }


    /**
    public static void main(String[] args){
        for(int i = 0; i <= 255; i++)
        {
            String n = "Â";

            System.out.format("%1$-5d", i);
            System.out.format("%1$-2c", (char) i);
            System.out.println();
        }
    }
    **/


    private static String getNum(int n){
        String num = "";
        if(n == 10) {
            num = "A";
        }else if(n == 11) {
            num = "B";
        }else if(n == 12) {
            num = "C";
        }else if(n == 13) {
            num = "D";
        }else if(n == 14) {
            num = "E";
        }else if(n == 15) {
            num = "F";
        }else{
            num = "" + (n);
        }
        return num;
    }
}

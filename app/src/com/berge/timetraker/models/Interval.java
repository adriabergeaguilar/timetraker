package com.berge.timetraker.models;

import com.berge.timetraker.Log;
import com.berge.timetraker.models.decorator.ProgramTask;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Esta clase es la que representa la diferencia entre dos momentos diferentes en el tiempo
 * esta clase implementa el patron Observer junto al Rellotge, de esta manera cada cierto
 * tiempo, es actualizada la fecha final del intervalo, asi asta que la tarea es parada.
 * De esta manera los datos son guardados en todo momento
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     java.util.Observer
 * @see     java.io.Serializable
 * @see     com.berge.timetraker.models.decorator.ProgramTask
 */
public class Interval implements Observer, Serializable {
    /**
     * Esta variable es la que identifica el padre para poder hacer referencia a el.
     */
    private ProgramTask parent;

    /**
     * fecha de inicio del intervalo
     */
    private Date init;
    /**
     * fecha de final del intervalo
     */
    private Date fin;

    public Interval(Date init) {
        this(init,init);
    }

    public Interval(Date init, Date fin) {
        super();
        this.init = init;
        this.fin = fin;
    }

    public void setParent(ProgramTask parent) {
        this.parent = parent;
    }

    public Date getInit() {
        return init;
    }

    public void setInit(Date init) {
        this.init = init;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public long getDurada() {
        return getDurada(fin);
    }

    /**
     * devuelve en milisegundos el tiempo transcurrido entre el inicio de el mismo,
     * junto con el finTemp
     * @param finTemp
     * @return
     */
    public long getDurada(Date finTemp){
        Calendar c = Calendar.getInstance();

        Calendar fechaInicio = new GregorianCalendar();

        fechaInicio.setTime(init);

        Calendar fechaFin = new GregorianCalendar();

        fechaFin.setTime(finTemp);

        c.setTimeInMillis(fechaFin.getTime().getTime() - fechaInicio.getTime().getTime());


        long dif =  c.getTime().getTime();
        //long time = Math.round((double)(dif/1000));

        long time = TimeUnit.MILLISECONDS.toSeconds(dif);

        Log.debug("*******************************\ninit: " + init + "\nfin: " + fin + "\ndiferencia: " + dif + "\nseconds: " + time + "\n*******************************");

        return time * 1000;
    }

    @Override
    public String toString() {
        return "Interval{" +
                "init=" + init +
                ", fin=" + fin +
                '}';
    }

    @Override
    public void update(Observable o, Object arg) {
        if(parent != null) {
            Log.debug("interval update of task : " + parent.getName());
            Date fin = (Date)arg;

            int limited = parent.getMaxLimitedDay(fin);
            if(limited != ProgramTask.UNLIMITED){
                int durada = (int)parent.getDurada(fin);
                if(durada < limited){
                    setFin(fin);
                }else{
                    try {
                        parent.stop();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }else {
                setFin(fin);
            }
        }
    }
}

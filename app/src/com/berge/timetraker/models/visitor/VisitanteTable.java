package com.berge.timetraker.models.visitor;

import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.Task;
import com.berge.timetraker.models.decorator.DecoradorLimitedTime;
import com.berge.timetraker.models.decorator.DecoradorNextTask;
import com.berge.timetraker.models.decorator.DecoradorProgramTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Este DecoradorTask es el que permite iniciar una tarea distinta en cuanto ella misma
 * ha terminado de ejecutarse, tanto si lo ha parado el usuario como de forma automatica
 *
 * @author  Ferran Muntada
 * @author  Adrià Bergé
 * @see     com.berge.timetraker.models.decorator.DecoradorTask
 */
public class VisitanteTable implements Visitante{
    @Override
    public void init() {
        String n = "┬";
        System.out.println("|-----" + n + "----" + n + "---------------------------------" + n + "----------------------------------" + n + "---------|");
        System.out.println("|Nom  | Id  | Temps Inicial                    | Temps Final                      | Durada   |");
        System.out.println("|-----+-----+----------------------------------+----------------------------------+----------|");
    }

    @Override
    public void finish() {

    }

    @Override
    public void visita(Projecte projecte) {
        Date init = projecte.getTimeToStartCount();
        Date fin = projecte.getTimeToStopCount();
        System.out.format("|%4s | %3s | %32s | %32s | %8s |",
                projecte.getName(),
                projecte.getId(),
                init == null ? "" : init,
                fin == null ? "" : fin,
                getDurada(projecte.getDurada()));
        System.out.println();
    }

    @Override
    public void visita(DecoradorNextTask decoradorNextTask) {

    }

    @Override
    public void visita(DecoradorProgramTime decoradorProgramTime) {

    }

    @Override
    public void visita(DecoradorLimitedTime decoradorLimitedTime) {

    }

    @Override
    public void visita(Task task) {
        Date init = task.getTimeToStartCount();
        Date fin = task.getTimeToStopCount();
        System.out.format("|%4s | %3s | %32s | %32s | %8s |",
                task.getName(),
                task.getId(),
                init == null ? "" : init,
                fin == null ? "" : fin,
                getDurada(task.getDurada()));
        System.out.println();
    }

    /**
     * formatea el tiempo representado por un numero en miliseguntos en un formato HH:MM:SS
     * @param date
     * @return
     */
    private String getDurada(long date){
        return getDurada(new Date(date));
    }

    /**
     * formatea el tiempo representado por una fecha en un formato HH:MM:SS
     * @param date
     * @return
     */
    private String getDurada(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.setTimeZone (TimeZone.getTimeZone("GMT"));

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        dateFormat.setCalendar(calendar);

        String durada = dateFormat.format(date);
        return durada;
    }
}

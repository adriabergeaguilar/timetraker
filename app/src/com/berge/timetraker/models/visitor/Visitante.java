package com.berge.timetraker.models.visitor;

import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.Task;
import com.berge.timetraker.models.decorator.DecoradorLimitedTime;
import com.berge.timetraker.models.decorator.DecoradorNextTask;
import com.berge.timetraker.models.decorator.DecoradorProgramTime;


/**
 * Esta interface es la que permitira recorrer a todos nuestros objetos
 * de manera que tendremos nuevas funcionalidades sin ser modificados cada uno de los objetos
 *
 * @author  Ferran Muntada
 * @author  Adrià Bergé
 * @see com.berge.timetraker.models.Projecte
 * @see com.berge.timetraker.models.Task
 * @see com.berge.timetraker.models.decorator.DecoradorNextTask
 * @see com.berge.timetraker.models.decorator.DecoradorLimitedTime
 * @see com.berge.timetraker.models.decorator.DecoradorProgramTime
 */
public interface Visitante {
    /**
     * esta es la funcion que se ejecuta antes de empezar hacer la visita por todos los elementos
     */
    public void init();

    /**
     * esta es la funcion q ue se ejecutara despues de hacer la vidita  a todos los elementos
     */
    public void finish();

    /**
     * visita a un objeto de tipo projecte
     * @param projecte
     */
    public void visita(Projecte projecte);

    /**
     * visita al objeto de tipo siguiente taska
     * @param decoradorNextTask
     */
    public void visita(DecoradorNextTask decoradorNextTask);

    /**
     * visita el programador de tiempos
     * @param decoradorProgramTime
     */
    public void visita(DecoradorProgramTime decoradorProgramTime);

    /**
     * visita el limitador
     * @param decoradorLimitedTime
     */
    public void visita(DecoradorLimitedTime decoradorLimitedTime);

    /**
     * visita la tarea como tal
     * @param task
     */
    public void visita(Task task);

}

package com.berge.timetraker.models.visitor;

import com.berge.timetraker.models.Actividad;
import com.berge.timetraker.models.Interval;
import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.Task;
import com.berge.timetraker.models.decorator.DecoradorLimitedTime;
import com.berge.timetraker.models.decorator.DecoradorNextTask;
import com.berge.timetraker.models.decorator.DecoradorProgramTime;
import com.berge.timetraker.models.decorator.ProgramTask;

import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Adria on 15/10/2014.
 */
public class VisitanteConsole implements Visitante {
    private boolean anteriorProject = true;
    private ProgramTask programTask;

    @Override
    public void init() {
    }

    @Override
    public void finish() {

    }

    @Override
    public void visita(Projecte projecte) {
        //if(!anteriorProject){
        System.out.println();
        //}
        tabular(projecte);
        System.out.print("Project: Nom("+projecte.getName()+") Temps("+getDurada(projecte.getDurada())+")");
        anteriorProject = true;
    }

    @Override
    public void visita(DecoradorNextTask decoradorNextTask) {
        anteriorProject = false;
        antecesor(decoradorNextTask);
        //tabular(decoradorNextTask);
        System.out.print("\tProxima Tasca(" + decoradorNextTask.getNextTask().getName() + ")");
    }

    @Override
    public void visita(DecoradorProgramTime decoradorProgramTime) {
        anteriorProject = false;
        antecesor(decoradorProgramTime);
        //tabular(decoradorProgramTime);
        HashMap<Integer,Date> days = decoradorProgramTime.getDays();

        String dies = "";
        ArrayList<Integer> daysPreSort = new ArrayList<Integer>(days.keySet());
        daysPreSort.add(8);
        daysPreSort.remove((Integer) (1));
        Collections.sort(daysPreSort);

        for(int dia : daysPreSort){
            switch (dia){
                case 8:
                    dies = dies + "dom";
                    break;
                case 2:
                    dies = "ln, " + dies;
                    break;
                case 3:
                    dies = dies + "mar, ";
                    break;
                case 4:
                    dies = dies + "mie, ";
                    break;
                case 5:
                    dies = dies + "jue, ";
                    break;
                case 6:
                    dies = dies + "vie, ";
                    break;
                case 7:
                    dies = dies + "sab, ";
                    break;
            }
        }

        Date date = (Date) days.values().toArray()[0];
        DateFormat dateFormat = new SimpleDateFormat(" dd-MM-yyyy HH:mm:ss");

        System.out.print("\tPrograma(" + dies + ")\tHora(" + dateFormat.format(date) + ")");
    }

    @Override
    public void visita(DecoradorLimitedTime decoradorLimitedTime) {
        anteriorProject = false;
        antecesor(decoradorLimitedTime);
        //tabular(decoradorLimitedTime);
        Date d = new Date (decoradorLimitedTime.getMaxLimitedDay(new Date()));

        System.out.print("\tLimit(" + getDurada(d) + ")");
    }

    @Override
    public void visita(Task task) {
        anteriorProject = false;
        antecesor(task);
        tabular(task);
        programTask = task;

        System.out.print("Task: Nom("+task.getName()+") Durada("+getDurada(task.getDurada())+")");

        if(task.getIntervals().size() > 0){
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            for(Interval interval : task.getIntervals()) {
                System.out.println();
                tabular(task);
                System.out.print("\t" + dateFormat.format(interval.getInit()) + " - " + dateFormat.format(interval.getFin()));
            }
        }
    }


    private void antecesor(ProgramTask programTask) {
        if(this.programTask != null && this.programTask.equals(programTask)){

        }else{
            System.out.println();

        }
    }

    private String getDurada(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.setTimeZone (TimeZone.getTimeZone("GMT"));

        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        dateFormat.setCalendar(calendar);

        String durada = dateFormat.format(date);
        return durada;
    }

    private String getDurada(long date){
        return getDurada(new Date(date));
    }


    private void tabular(Actividad actividad){
        String tab = "\t";
        int count = countParents(actividad);

        String tabs = "";

        for(int i=0 ; i != count; i++){
            tabs += tab;
        }

        System.out.print(tabs);

    }

    private int countParents(Actividad actividad){
        if(actividad.getParent() == null){
            return 1;
        }else
            return countParents(actividad.getParent()) + 1;
    }
}

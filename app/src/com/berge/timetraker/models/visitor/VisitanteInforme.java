package com.berge.timetraker.models.visitor;

import com.berge.timetraker.models.Actividad;
import com.berge.timetraker.models.Interval;
import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.Task;
import com.berge.timetraker.models.decorator.DecoradorLimitedTime;
import com.berge.timetraker.models.decorator.DecoradorNextTask;
import com.berge.timetraker.models.decorator.DecoradorProgramTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by adria on 21/10/14.
 */
public class VisitanteInforme implements Visitante {
    private Date init;
    private Date fin;

    private List<String> lineas;
    private List<String> projectesArrel;
    private List<String> subProjectes;
    private List<String> taskes;
    private List<String> intervels;
    private SimpleDateFormat simpleDateFormat;
    private SimpleDateFormat simpleHourFormat;

    public VisitanteInforme(Date init, Date fin){
        this.init = init;
        this.fin = fin;
        lineas = new ArrayList<String>();
        projectesArrel = new ArrayList<String>();
        subProjectes = new ArrayList<String>();
        taskes = new ArrayList<String>();
        intervels = new ArrayList<String>();
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        simpleHourFormat = new SimpleDateFormat(("HH mm ss"));
    }

    public void init() {

        String[] lines = new String[]{
                "------------------------------------------------------------------------------------------------------",
                "Informe detallat",
                "------------------------------------------------------------------------------------------------------",
                "Perióde",
                "\tData",
                "Desde " + simpleDateFormat.format(init) + "h",
                "Fins a " + simpleDateFormat.format(fin) + "h",
                "Data de generació de línforme " + simpleDateFormat.format(new Date()) + "h",
                "------------------------------------------------------------------------------------------------------"
        };

        for(String s : lines){
            lineas.add(s);
        }

    }

    public void finish() {
        lineas.add("Projectes arrel");
        lineas.add("No.\t Projecte \t Data d’inici \t Data final \t Temps total");
        for(String s : projectesArrel){
            lineas.add(s);
        }
        lineas.add("------------------------------------------------------------------------------------------------------");
        lineas.add("Subprojectes");
        lineas.add("No. Projecte \t Data d’inici \t Data final \t Temps total");
        for(String s : subProjectes){
            lineas.add(s);
        }
        lineas.add("------------------------------------------------------------------------------------------------------");
        lineas.add("Tasques");
        lineas.add("No.(sub) Projecte \t Tasca \t Data d’inici \t Data final \t Temps total");
        for(String s : taskes){
            lineas.add(s);
        }
        lineas.add("------------------------------------------------------------------------------------------------------");
        lineas.add("Intervals");
        lineas.add("No.(sub) Projecte \t Tasca \t Interval \t Data d’inici \t Data final \t Durada");
        for(String s : intervels){
            lineas.add(s);
        }
        lineas.add("------------------------------------------------------------------------------------------------------");


        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        for(String s : lineas){
            System.out.println(s);
        }
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

    }

    public void visita(Projecte projecte) {
        if(projecte.getParent() == null) {//base

        }else if(projecte.getParent().getParent() == null){//projectes
            Date initFecha = projecte.getTimeToStartCount();
            Date finFecha = projecte.getTimeToStopCount();
            String id = getFormatID(projecte);
            projectesArrel.add(
                    id + "\t" +
                    projecte.getName() + "\t" +
                    (initFecha != null ? simpleDateFormat.format(initFecha) : "") + "\t" +
                    (finFecha != null ? simpleDateFormat.format(finFecha) : "") + "\t" +
                    simpleHourFormat.format(new Date(projecte.getDurada()))
            );
        }else{//subprojectes
            Date initFecha = projecte.getTimeToStartCount();
            Date finFecha = projecte.getTimeToStopCount();
            String id = getFormatID(projecte);
            subProjectes.add(
                    id + "\t" +
                            projecte.getName() + "\t" +
                            (initFecha != null ? simpleDateFormat.format(initFecha) : "") + "\t" +
                            (finFecha != null ? simpleDateFormat.format(finFecha) : "") + "\t" +
                            simpleHourFormat.format(new Date(projecte.getDurada()))
            );
        }
    }


    private String getFormatID(Actividad actividad){
        if(actividad.getParent() != null) {
            return getFormatID(actividad.getParent()) + "." + actividad.getId();
        }else{
            return actividad.getId()+"";
        }
    }
    public void visita(DecoradorNextTask decoradorNextTask) {

    }

    public void visita(DecoradorProgramTime decoradorProgramTime) {

    }

    public void visita(DecoradorLimitedTime decoradorLimitedTime) {

    }

    public void visita(Task task) {
        //No.(sub) Projecte Tasca Data d’inici Data final Temps total

        Date initFecha = task.getTimeToStartCount();
        Date finFecha = task.getTimeToStopCount();
        String id = getFormatID(task.getParent());
        taskes.add(
                id + "\t" +
                task.getName() + "\t" +
                (initFecha != null ? simpleDateFormat.format(initFecha) : "") + "\t" +
                (finFecha != null ? simpleDateFormat.format(finFecha) : "") + "\t" +
                simpleHourFormat.format(new Date(task.getDurada()))
        );

        List<Interval> intervals = task.getIntervals();
        for (int i = 0; i != intervals.size(); i++){
            Interval interval = intervals.get(i);
            Date in = interval.getInit();
            Date fi = interval.getFin();
            intervels.add(
                    id + "\t" +
                    task.getName() + "\t" +
                    i + "\t"+
                    (initFecha != null ? simpleDateFormat.format(in) : "") + "\t" +
                    (finFecha != null ? simpleDateFormat.format(fi) : "") + "\t" +
                    simpleHourFormat.format(new Date(interval.getDurada()))
            );
        }
    }
}

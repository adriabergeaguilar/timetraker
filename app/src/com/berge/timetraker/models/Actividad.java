package com.berge.timetraker.models;

import com.berge.timetraker.models.visitor.Visitante;
import com.berge.timetraker.utils.GeneradorID;

import java.io.Serializable;
import java.util.Date;
/**
 * Esta clase abstracta es para la gestion de tareas y proyectos
 * usando el patron Composite hacemos que compartan los datos similares
 * y al mismo tiempo ayuda a una simplificacion de codigo, esta es la unidad
 * mas global del proyecto la cual sin ella, no se comprende
 *
 * Al mismo tiempo usamos el patron serializable en este punto porque
 * es el objeto abstracto base para poder guardar.
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     Date
 * @see com.berge.timetraker.models.Projecte
 * @see com.berge.timetraker.utils.GeneradorID
 */
public abstract class Actividad implements Serializable{
    /**
     * Proyecto padre de la actividad
     */
    private Projecte parent;
    /**
     * nombre de la actividad
     */
    private String name = null;
    /**
     * descripcion de la actividad
     */
    private String desc = null;
    /**
     * id de la actividad
     */
    private int id = -1;

    public Actividad(String name, String desc){
        GeneradorID generador = GeneradorID.getInstancia();
        id = generador.getSimpleID();
        this.name = name;
        this.desc = desc;
    }

    /**
     * devuelve en milisegundos el timepo que ha transcurrido en esta actividad
     * @return
     */
    public abstract long getDurada();

    /**
     * metodo el cual aceptas al visitante
     * @param visitante
     */
    public abstract  void acceptVisitante(Visitante visitante);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Projecte getParent() {
        return parent;
    }

    /**
     * este metodo devuelve la actividad de mas alto nivel, aquella que
     * es el padre de todos, siendo la que no tiene padre,
     * si se hiciera una cuncurrencia en la insercion de padres,
     * dara un error de StockOverFlow
     * @return
     */
    public Actividad getParentBase(){
        if(parent != null){
            return parent.getParentBase();
        }else {
            return this;
        }
    }

    public void setParent(Projecte parent) {
        this.parent = parent;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * esta funcion retorna el primer momento en que se  inicio esta actividad
     * @return
     */
    public abstract Date getTimeToStartCount();

    /**
     * esta funcion devuelve el ultimo momento en el que se conto el tiempo
     * @return
     */
    public abstract Date getTimeToStopCount();


}

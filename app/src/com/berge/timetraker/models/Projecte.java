package com.berge.timetraker.models;

import com.berge.timetraker.Log;
import com.berge.timetraker.comparators.ComparatorMaxDate;
import com.berge.timetraker.comparators.ComparatorMinDate;
import com.berge.timetraker.models.visitor.Visitante;
import com.berge.timetraker.utils.GeneradorID;

import java.util.*;

/**
 * Esta clase es la que representa un proyecto, con la peculiaridad de que el
 * puede contener a objetos de tipo actividad, dando la opcion de crear un arbol
 * usando el patron Composite
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     Actividad
 */
public class Projecte extends Actividad {
    /**
     * Lista de actividades que contiene un proyecto, esot es lo que genera la idea de composite
     */
    private List<Actividad> actividades;

    public Projecte(String name,String desc) {
        super(name,desc);
        actividades = new ArrayList<Actividad>();
    }

    @Override
    public long getDurada() {

        int count = 0;
        for(Actividad actividad : actividades){
            count += actividad.getDurada();
        }
        return count;
    }

    @Override
    public void acceptVisitante(Visitante visitante) {
        if(getParent() == null){
            visitante.init();
        }
        visitante.visita(this);
        for(Actividad actividad : actividades){
            actividad.acceptVisitante(visitante);
        }
        if(getParent() == null){
            visitante.finish();
        }
    }

    @Override
    public Date getTimeToStartCount() {
        List<Date> fechas = new ArrayList<Date>();
        for(Actividad actividad : actividades){
            Date date = actividad.getTimeToStartCount();
            fechas.add(date);
        }

        Collections.sort(fechas, new ComparatorMaxDate());
        if(fechas.size() > 0 ) {
            return fechas.get(0);
        }else {
            return null;
        }
    }

    @Override
    public Date getTimeToStopCount() {
        List<Date> fechas = new ArrayList<Date>();
        for(Actividad actividad : actividades){
            Date date = actividad.getTimeToStopCount();
            fechas.add(date);
        }

        Collections.sort(fechas,new ComparatorMinDate());
        if(fechas.size() > 0 ) {
            return fechas.get(0);
        }else {
            return null;
        }
    }

    /**
     * añade actividades a un proyecto, siendo asi puede añadir nuevos proyectos
     * como tareas
     * @param actividad
     * @return
     */
    public boolean addActividad(Actividad actividad){
        Log.debug("in project : " + this.getName() + " add " + actividad.getName());
        actividad.setParent(this);

        try {
            GeneradorID generadorID = GeneradorID.getInstancia();
            int id = generadorID.getID(this,actividad);
            actividad.setId(id);
        } catch (Exception e) {
            e.printStackTrace();
            Log.error("generacion de ids : " + e.getMessage());
        }

        return actividades.add(actividad);
    }

    /**
     * con este metodo eliminamos la actividad de el mismo
     * @param actividad
     * @return
     */
    public boolean removedActividad(Actividad actividad){
        Log.debug("in project : " + this.getName() + " removed " + actividad.getName());
        actividad.setParent(null);
        return actividades.add(actividad);
    }

    public List<Actividad> getActividades(){
        return actividades;
    }



}

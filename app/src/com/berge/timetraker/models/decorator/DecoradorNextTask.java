package com.berge.timetraker.models.decorator;

import com.berge.timetraker.models.visitor.Visitante;

import java.util.Date;

/**
 * Este DecoradorTask es el que permite iniciar una tarea distinta en cuanto ella misma
 * ha terminado de ejecutarse, tanto si lo ha parado el usuario como de forma automatica
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     com.berge.timetraker.models.decorator.DecoradorTask
 */
public class DecoradorNextTask extends DecoradorTask {
    private boolean isStoped = true;
    /**
     * es la tarea que se ejecutara al parar la primera
     */
    private ProgramTask nextTask;

    public DecoradorNextTask(ProgramTask programTask) {
        super(programTask);
    }

    /**
     * devuelve la tarea que se deve ejecutar al ser parada esta
     * @return
     */
    public ProgramTask getNextTask() {
        return nextTask;
    }

    /**
     * modifica la tarea que sera ejecutada al parar esta
     * @param nextTask
     */
    public void setNextTask(ProgramTask nextTask) {
        this.nextTask = nextTask;
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        if(isStoped) {
            if (nextTask != null) {
                nextTask.init();
                isStoped = false;
            } else {
                throw new Exception("Not programer next task");
            }
        }
    }

    @Override
    public void acceptVisitante(Visitante visitante) {
        super.acceptVisitante(visitante);
        visitante.visita(this);
    }

}
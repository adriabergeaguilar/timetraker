package com.berge.timetraker.models.decorator;

import com.berge.timetraker.models.visitor.Visitante;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


/**
 * Este DeciradorTask es el que permite programar tareas de forma periodica
 * de manera que los dias que queramos a una hora concreta se realizara una tarea
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     com.berge.timetraker.models.decorator.DecoradorTask
 */
public class DecoradorProgramTime extends DecoradorTask  {
    /**
     * Hash de los dias de la semana para poder programar la tarea
     */
    private HashMap<Integer,Date> days;
    public DecoradorProgramTime(ProgramTask programTask) {
        super(programTask);
        this.days = new HashMap<Integer, Date>();
    }

    /**
     * Guarda el dia de la semana que quieres programar, debe ser la representacion de un dia de la semana
     * no puede ser de un dia concreto.
     * @param day
     */
    public void addDay(Date day){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(day);

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        days.put(dayOfWeek,day);
    }

    /**
     * devuelve una lista hash con los dias de la semana que as programado
     * @return
     */
    public HashMap<Integer, Date> getDays() {
        return days;
    }

    /**
     * esta funcion dado un dia de la semana te dice los dias que faltan para el siguiente dia.
     * @param dayOfWeek
     * @return
     */
    private int getNumDaysToNext(int dayOfWeek){
        if(days.size() > 0 && days.size() <= 7) {
            if (days.containsKey(dayOfWeek)) {
                return 0;
            } else {
                dayOfWeek++;
                if (dayOfWeek == 8) {
                    dayOfWeek = 1;
                }
                return getNumDaysToNext(dayOfWeek) + 1;
            }
        }else{
            return 0;
        }
    }

    public Date getFirstTime() throws Exception {
        if(days.size() < 0){
            throw new Exception("Falta la lista de dias de la semana a programar");
        }
        //System.out.println("debug: DecoradorProgramTime\t" + "now:" + new Date());
        Date now = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        int numDaysToNext = getNumDaysToNext(dayOfWeek);


        calendar.add(Calendar.DATE,numDaysToNext);
        calendar.setTime(days.get(calendar.get(Calendar.DAY_OF_WEEK)));

        Calendar date = Calendar.getInstance();
        date.set(Calendar.DAY_OF_WEEK,calendar.get(Calendar.DAY_OF_WEEK));
        date.set(Calendar.HOUR, calendar.get(Calendar.HOUR));
        date.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        System.out.println(date.getTime());

        return date.getTime();

    }

    @Override
    public void acceptVisitante(Visitante visitante) {
        super.acceptVisitante(visitante);
        visitante.visita(this);
    }


}

package com.berge.timetraker.models.decorator;

import com.berge.timetraker.models.Interval;
import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.visitor.Visitante;

import java.util.Date;
import java.util.List;

/**
 *Esta interficie es la descripcion de como deven ser nuestras tareas para poder ser
 * programadas segun pultiples escenarios. Configurara una variable Timer de como se
 * deve programar en el tiempo segun una fecha, una repeticion y demas.
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     Interval
 * @see     Projecte
 * @see     Visitante
 * @see     Exception
 * @see     List
 * @see     Date
 */
public interface ProgramTask {

    public static final int UNLIMITED = 0;

    /**
     * retorna el nombre de programTask
     * @return el nombre
     */
    public String getName();

    /**
     * Devuelve un objeto Date que representa el momento que debe empezar la
     * tarea que queremos programar
     * @return es el valor en milisegundos del momento de inicio.
     * @throws Exception
     */
    public Date getFirstTime() throws Exception;

    /**
     * Este valor en milisegundos representa cada cuantos milisegundos se repetira la tarea
     * este valor siempre deve ser mayor que 0
     * @return un tiempo representado en milisegundos
     */
    public int getPeriod();

    /**
     * Esto indica la limitacion de tiempo que se tiene para esta tarea
     * en un dia concreto, de manera que puede ser una tarea periodica.
     * @param day
     * @return devuelve en milisegundos el tiempo maximo
     */
    public int getMaxLimitedDay(Date day);

    /**
     * Inserta un interval de temps a la tasca per tal de poder portar el
     * recompte de tot el temps que s ha d estinat a la taska
     * @param interval
     */
    public void insertInterval(Interval interval);

    /**
     * Inicia el recuento de tiempo de esta tarea , con todo
     * lo que ello comporta
     * @throws Exception
     */
    public void init() throws Exception;

    /**
     * Esto permite iniciar una tarea con el efecto cascada de solapamiento de configuraciones
     * dado al patron de diseño Decorator, de esta manera podemos aplicar el patron para
     * toda la configuracion de la tarea y su inicio
     * @param programTask
     * @throws Exception
     */
    public void init(ProgramTask programTask) throws Exception;

    /**
     * Esta funcion es la que ara que paremos el contador de tiempo de una tarea,
     * asi controlamos el flujo de travajo y podemos dar la orden al contador
     * de que no sume los tiempos.
     * @throws Exception
     */
    public void stop() throws Exception;

    /**
     * Esto permite parar una tarea
     *
     * @param programTask
     * @throws Exception
     */
    public void stop(ProgramTask programTask) throws Exception;

    /**
     * devuelve una lista de intervalos que ha tenido esta tarea asta el momento
     * son cada uno de los elementos minimos de tiempo unidos de forma consecutiva
     * @return
     */
    public List<Interval> getIntervals();

    /**
     * devuelve un valor representado en milisegundos entre el tiempo de inicio y el de fin de
     * todos y cada unos de los intervalos
     * @return
     */
    public long getDurada();

    /**
     * este metodo modifica el parametro padre de manera que una tarea conoce en que proyecto esta y el
     * proyecto sabe todas sus tareas
     * @param parent
     */
    public void setParent(Projecte parent);

    /**
     * devuelve el objeto que representa nuestro padre
     * @return
     */
    public Projecte getParent();

    /**
     * nos devuelve en milisegundos el tiempo transcurrido de un momento condreto que le pasamos asta el
     * momento que se inicio el contador de tiempo.
     * @param day
     * @return
     */
    public long getDurada(Date day);

    /**
     * este metodo es el metodo que hace de enlace con el patron Visitor
     * para poder asi hacer multiples funciones sin ser modificado  nada.
     * @param visitante
     */
    public void acceptVisitante(Visitante visitante);
}

package com.berge.timetraker.models.decorator;

import com.berge.timetraker.models.visitor.Visitante;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Este DecoradorTask es el que permite limitar el tiempo de una tarea
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     com.berge.timetraker.models.decorator.DecoradorTask
 */
public class DecoradorLimitedTime extends DecoradorTask {
    /**
     * es el hash de los dias de la semana y la limitacion que le quieres proporcionar a cada dia
     */
    private HashMap<Integer,Integer> limiteds;

    public DecoradorLimitedTime(ProgramTask programTask) {
        super(programTask);
        this.limiteds = new HashMap<Integer, Integer>();
    }

    /**
     * añade una limitacion para el dia de la semana que se desea
     * @param dayOfWeek
     * @param limited
     */
    public void addLimiteds(int dayOfWeek, int limited){
        limiteds.put(dayOfWeek,limited);
    }

    /**
     * elimina el limite para un dia de la semana
     * @param dayOfWeek
     */
    public void removedLimiteds(int dayOfWeek){
        limiteds.remove(dayOfWeek);
    }

    @Override
    public int getMaxLimitedDay(Date day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(day);
        int datOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        return limiteds.get(datOfWeek);
    }

    @Override
    public void acceptVisitante(Visitante visitante) {
        super.acceptVisitante(visitante);
        visitante.visita(this);
    }

}

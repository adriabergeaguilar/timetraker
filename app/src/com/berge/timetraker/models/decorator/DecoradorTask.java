package com.berge.timetraker.models.decorator;

import com.berge.timetraker.models.Actividad;
import com.berge.timetraker.models.Interval;
import com.berge.timetraker.models.Projecte;
import com.berge.timetraker.models.visitor.Visitante;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *Esta clase abstracta es la que desarroya el patron Decorator
 * ya que nos permite a una tarea basica darle nuevas funcionalidades
 * segun los decoradores que le sumamos, que seran todas aquellas clases
 * que heredan de DecoradorTask
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     Interval
 * @see     Projecte
 * @see     Visitante
 * @see     Exception
 * @see     List
 * @see     Date
 * @see     java.io.Serializable
 * @see     com.berge.timetraker.models.Actividad
 */
public abstract class DecoradorTask extends Actividad implements ProgramTask, Serializable {
    /**
     * Es el elemento que hace que se genere el patron decorator, por tal de poder hacer la union
     * de funcionalidades de sus padres
     */
    private ProgramTask programTask;

    public DecoradorTask(ProgramTask programTask){
        super(null,null);
        this.programTask = programTask;
    }

    @Override
    public void setParent(Projecte parent) {
        programTask.setParent(parent);
    }

    @Override
    public Projecte getParent() {
        return programTask.getParent();
    }

    @Override
    public Date getFirstTime() throws Exception {
        return programTask.getFirstTime();
    }

    @Override
    public int getPeriod() {
        return programTask.getPeriod();
    }

    @Override
    public void insertInterval(Interval interval) {
        programTask.insertInterval(interval);
    }

    @Override
    public List<Interval> getIntervals() {
        return programTask.getIntervals();
    }

    @Override
    public long getDurada() {
        return programTask.getDurada();
    }

    @Override
    public String getName() {
        return programTask.getName();
    }

    @Override
    public int getMaxLimitedDay(Date day) {
        return programTask.getMaxLimitedDay(day);
    }

    @Override
    public void init() throws Exception {
        this.programTask.init(this);
    }

    @Override
    public void init(ProgramTask programTask) throws Exception {
        this.programTask.init(programTask);
    }

    @Override
    public void stop(ProgramTask programTask) throws Exception {
        this.programTask.stop(programTask);
    }

    @Override
    public void stop() throws Exception {
        this.programTask.stop();
    }

    @Override
    public long getDurada(Date day) {
        return programTask.getDurada(day);
    }

    @Override
    public void acceptVisitante(Visitante visitante) {
        programTask.acceptVisitante(visitante);
    }

    @Override
    public String toString() {
        return this.programTask.toString();
    }

    @Override
    public boolean equals(Object obj) {
        return this.programTask.equals(obj);
    }

    @Override
    public Date getTimeToStartCount() {
        return null;
    }

    @Override
    public Date getTimeToStopCount() {
        return null;
    }
}

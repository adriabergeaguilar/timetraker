package com.berge.timetraker.models;

import com.berge.timetraker.ConstantsUsuari;
import com.berge.timetraker.Log;
import com.berge.timetraker.Rellotge;
import com.berge.timetraker.comparators.ComparatorMaxDate;
import com.berge.timetraker.comparators.ComparatorMinDate;
import com.berge.timetraker.models.decorator.ProgramTask;
import com.berge.timetraker.models.visitor.Visitante;
import com.berge.timetraker.utils.Utils;

import java.util.*;

/**
 * Esta clase es parte del Composite entre Actividad y proyecto, esta da la peculiaridad de que
 * no puede contener objetos de su mismo typo, sino que el acumula intervalos de tiempo,
 * el es la unidad minima de organizacion
 *
 * @author  Adrià Bergé
 * @author  Ferran Muntada
 * @see     Actividad
 * @see     com.berge.timetraker.models.decorator.ProgramTask
 * @see     com.berge.timetraker.models.Interval
 */
public class Task extends Actividad implements ProgramTask {
    /**
     * Lista de intervalos que se han cronometrado de una tarea
     */
    private List<Interval> intervals;
    /**
     * maquina d estados para poder identificar si el momento es valido o no
     */
    private boolean isStoped = true;


    public Task(String name,String desc) {
        super(name,desc);
        intervals = new ArrayList<Interval>();
    }

    @Override
    public long getDurada() {
        long count = 0l;

        for(Interval interval : intervals){
            count += interval.getDurada();
        }

        return count;
    }

    @Override
    public void acceptVisitante(Visitante visitante) {
        visitante.visita(this);
    }

    public boolean removedInterval(Interval interval){
        return intervals.remove(interval);
    }

    @Override
    public long getDurada(Date day) {

        long count = 0l;

        for(Interval interval : intervals){
            if(Utils.isEqualsDay(day,interval.getInit())) {
                count += interval.getDurada();
            }
        }

        return count;
    }

    @Override
    public Date getFirstTime() {
        return new Date();
    }

    @Override
    public int getPeriod() {
        return ConstantsUsuari.getInstance().getSegonsRecompte() * 1000;
    }

    @Override
    public int getMaxLimitedDay(Date day) {
        return ProgramTask.UNLIMITED;
    }

    @Override
    public void insertInterval(Interval interval) {
        this.intervals.add(interval);
        Log.debug("add in task: " + this.getName() + " interval : " + interval);
    }

    @Override
    public void init(ProgramTask programTask) throws Exception {
        isStoped = false;
        Rellotge rellotge = Rellotge.getInstance(programTask,true);
        Log.debug("task : " + this.getName() + " init task : " + programTask.getName());
        //System.out.println(programTask.getName() + " init");
        rellotge.addTaska(programTask);
    }

    @Override
    public void init() throws Exception {
        this.init(this);
    }

    @Override
    public void stop() {
        this.stop(this);
    }

    @Override
    public void stop(ProgramTask programTask) {
        if(!isStoped) {
            isStoped = true;
            Rellotge rellotge = Rellotge.getInstance(programTask);
            //System.out.println(programTask.getName() + " stop");
            if (rellotge != null) {
                Log.debug("task : " + programTask.getName() + " is stoped");
                rellotge.removedTask(programTask);
                Rellotge.removed(programTask);
            }
        }

    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ProgramTask) {
            ProgramTask p = (ProgramTask)obj;
            return this.getName().equals(p.getName());
        }else {
            return false;
        }
    }

    @Override
    public List<Interval> getIntervals(){
        return intervals;
    }


    @Override
    public Date getTimeToStartCount() {
        List<Date> fechas = new ArrayList<Date>();
        for(Interval interval : intervals){
            Date date = interval.getInit();
            fechas.add(date);
        }

        Collections.sort(fechas, new ComparatorMaxDate());
        if(fechas.size() > 0 ) {
            return fechas.get(0);
        }else {
            return null;
        }
    }

    @Override
    public Date getTimeToStopCount() {
        List<Date> fechas = new ArrayList<Date>();
        for(Interval interval : intervals){
            Date date = interval.getFin();
            fechas.add(date);
        }

        Collections.sort(fechas, new ComparatorMinDate());
        if(fechas.size() > 0 ) {
            return fechas.get(0);
        }else {
            return null;
        }
    }
}
